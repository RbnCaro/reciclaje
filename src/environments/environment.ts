// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD3xnwv5il5hMkePBC4BvvhMd3rPumJE84",
    authDomain: "reciclaje-b57e6.firebaseapp.com",
    projectId: "reciclaje-b57e6",
    storageBucket: "reciclaje-b57e6.appspot.com",
    messagingSenderId: "970306749185",
    appId: "1:970306749185:web:66b472b882077ae8c35100"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
