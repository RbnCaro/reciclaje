import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-choose-profile',
  templateUrl: './choose-profile.component.html',
  styleUrls: ['./choose-profile.component.css']
})
export class ChooseProfileComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goTo(route: string, option?: string) {
    if (route == 'register') {
      localStorage.setItem("profileSelected", option);
      this.router.navigate(['register']);
    } else {
      this.router.navigate(['/']);
    }
  }

}
