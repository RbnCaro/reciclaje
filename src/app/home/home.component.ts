import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ConfirmationService]
})
export class HomeComponent implements OnInit {

  paperboard: number = 0;
  can: number = 0;
  glass: number = 0;
  plastic: number = 0;
  recycles = [];
  loading: boolean = true;

  constructor(private store: AngularFirestore, private confirmationService: ConfirmationService, private message: MessageService) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.loading = true;
    this.recycles = [];
    let result = this.store.collection('/recycle-pickup').snapshotChanges();
    result.subscribe((recycles: any) => {
      let profile = localStorage.getItem("profile");
      recycles.forEach(recycle => {
        console.log(profile);
        
        if (profile == 'collector') {
          if (recycle.payload.doc.data().state != 'created') {
            this.recycles.push({
              ...recycle.payload.doc.data(),
              id: recycle.payload.doc.id
            });
          }
        } else {
          this.recycles.push({
            ...recycle.payload.doc.data(),
            id: recycle.payload.doc.id
          });
        }
      });
      this.recycles = this.recycles.reverse();
      this.loading = false;
    });
  }

  confirmReception(recycle) {
    this.confirmationService.confirm({
      message: '¿Estás seguro/a que deseas confirmar la recepción?',
      header: 'Confirmación',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store.collection('/recycle-pickup').doc(recycle.id).set({
          ...recycle,
          state: 'done'
        }).then(() => {
          this.message.add({
            severity: "success",
            summary: "'Exito'",
            detail: "La recepción se ha realizado con éxito",
            closable: false,
            life: 1500
          });
          this.loadData();
        })
      },
    });
  }

  cancelRecycle(recycle) {
    this.confirmationService.confirm({
      message: '¿Estás seguro/a que deseas cancelar el pedido?',
      header: 'Cancelar',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.loading = true;
        this.store.collection('/recycle-pickup').doc(recycle.id).set({
          ...recycle,
          state: 'cancel'
        }).then(() => {
          this.message.add({
            severity: "success",
            summary: "'Exito'",
            detail: "La recepción se ha cancelado con éxito",
            closable: false,
            life: 1500
          });
          this.loadData();
        })
      },
    });
  }
}
