import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { PrinicpalComponent } from './prinicpal/prinicpal.component';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { ScheduleRecycleComponent } from './schedule-recycle/schedule-recycle.component';
import { SuccessPageComponent } from './success-page/success-page.component';
import { ConfirmationRequestComponent } from './confirmation-request/confirmation-request.component';
import { RecycleQuantityComponent } from './recycle-quantity/recycle-quantity.component';
import { UserDataComponent } from './user-data/user-data.component';
import { HomeComponent } from './home/home.component';
import { ModifyUserDataComponent } from './modify-user-data/modify-user-data.component';
import { ChooseProfileComponent } from './choose-profile/choose-profile.component';
import { RecycleChooseComponent } from './recycle-choose/recycle-choose.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';

const routes: Routes = [
  { path: '', component: LoginComponent, data: {animation: 'LoginPage'}},
  { 
    path: 'principal', 
    component: PrinicpalComponent,
    children: [
      { 
        path: 'reciclar-elegir', 
        component: RecycleChooseComponent, 
        outlet: 'asd',
        data: {animation: 'LoginPage'},
      },
      { 
        path: 'reciclar', 
        component: RecycleQuantityComponent, 
        outlet: 'asd',
        data: {animation: 'LoginPage'},
      },
      { 
        path: 'agendarReciclaje',
        component: ScheduleRecycleComponent,
        outlet: 'asd'
      },
      { path: 'exito', component: SuccessPageComponent, outlet: 'asd'},
      { path: 'confirmacion-pedido', component: ConfirmationRequestComponent, outlet: 'asd'},
      { 
        path: 'inicio', 
        component: HomeComponent, 
        outlet: 'asd',
        data: {animation: 'RegisterPage'}
      },
      { 
        path: 'user-data', 
        component: UserDataComponent, 
        outlet: 'asd',
        data: {animation: 'RegisterPage'}
      },
      { 
        path: 'modificarDatosUsuario', 
        component: ModifyUserDataComponent, 
        outlet: 'asd',
      },
    ]
  },
  { path: 'register', component: RegisterComponent, data: {animation: 'RegisterPage'}},
  { path: 'admin-panel', component: AdminPanelComponent},
  { path: 'choose-profile', component: ChooseProfileComponent, data: {animation: 'RegisterPage'}},
]

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
