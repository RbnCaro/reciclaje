import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';

import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  // providers: [MessageService]
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  isLoading: boolean = false;

  constructor(private router: Router, private store: AngularFirestore, private message: MessageService) { }

  ngOnInit(): void {
  }

  login() {
    if (!this.password || !this.email) {
      this.message.add({
        severity: "warn",
        summary: "Campos faltantes",
        detail: "Por favor ingrese correo y/o contraseña",
        closable: false,
        life: 3000
      });
      return;
    }
    this.isLoading = true;
    let result = this.store.collection('/users', ref => ref.where('email', '==', this.email)).snapshotChanges();
    result.subscribe((users: any) => {
      console.log(users);
      if (users.length == 0 || users[0].payload.doc.data().password != this.password) {
        this.message.add({
          severity: "error",
          summary: "Validación",
          detail: "Usuario y/o contraseña incorrecta",
          closable: false,
          life: 3000
        });
        this.isLoading = false;
        return false;
      }
      localStorage.setItem("email", this.email);
      localStorage.setItem("userId", users[0].payload.doc.id);
      localStorage.setItem("profile", users[0].payload.doc.data().profile);
      localStorage.setItem("user", JSON.stringify(users[0].payload.doc.data()));
      if (users[0].payload.doc.data().profile == 'recycler') {
        this.router.navigate(['principal']);
      } else  if (users[0].payload.doc.data().profile == 'collector') {
        this.router.navigate(['principal']);
      } else  if (users[0].payload.doc.data().profile == 'admin') {
        this.router.navigate(['admin-panel']);
      }
    })
  }

  register(): void {
    this.router.navigate(['register']);
  }
}
