import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.css']
})
export class UserDataComponent implements OnInit {
  user: any = {
    name: "",
    lastName: "",
    email: "",
    phone: "",
    address: "",
    region: "",
    commune: "",
    points: 0
  };

  constructor(private router: Router, private store: AngularFirestore, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    let email = localStorage.getItem("email");
    let result = this.store.collection('/users', ref => ref.where('email', '==', email)).valueChanges();
    result.subscribe((users: User[]) => {
      console.log(users);
      this.user = users[0];
    });
  }

  modifyUserData() {
    this.router.navigate([{ outlets: { asd: ["modificarDatosUsuario"] } }], { relativeTo: this.activatedRoute.parent });
  }

  closeSession() {
    this.router.navigate(['/']);
  }
}
