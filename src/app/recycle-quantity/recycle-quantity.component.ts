import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-recycle-quantity',
  templateUrl: './recycle-quantity.component.html',
  styleUrls: ['./recycle-quantity.component.css']
})
export class RecycleQuantityComponent implements OnInit {
  paperboard: number = 0;
  plastic: number = 0;
  glass: number = 0;
  can: number = 0;
  items: MenuItem[];

  constructor(private router: Router, private message: MessageService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.items = [{
        label: 'File',
        items: [
            {label: 'New', icon: 'pi pi-plus'},
            {label: 'Open', icon: 'pi pi-download'}
        ]
    },
    {
        label: 'Edit',
        items: [
            {label: 'Undo', icon: 'pi pi-refresh'},
            {label: 'Redo', icon: 'pi pi-repeat'}
        ]
    }];
  }

  goToDate() {
    let total: number = Number(this.paperboard) + Number(this.plastic) + Number(this.glass) + Number(this.can);
    if (total < 20) {
      this.message.add({
        severity: "warn",
        summary: "No hay suficientes items",
        detail: "Cantidad mínima a reciclar: 20 items\n Cantidad actual: " + total,
        closable: false,
        life: 3000
      });
      return;
    }
    localStorage.removeItem('paperboard');
    localStorage.removeItem('plastic');
    localStorage.removeItem('glass');
    localStorage.removeItem('can');
    if (this.paperboard != 0) {
      localStorage.setItem('paperboard', this.paperboard.toString());
    }
    if (this.plastic != 0) {
      localStorage.setItem('plastic', this.plastic.toString());
    }
    if (this.glass != 0) {
      localStorage.setItem('glass', this.glass.toString());
    }
    if (this.can != 0) {
      localStorage.setItem('can', this.can.toString());
    }
    // this.router.navigate(['/agendar-reciclaje']);
    // console.log(this.activatedRoute.url)
    this.router.navigate([{ outlets: { asd: ["agendarReciclaje"] } }], { relativeTo: this.activatedRoute.parent });
  }
}
