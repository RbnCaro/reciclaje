import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecycleQuantityComponent } from './recycle-quantity.component';

describe('RecycleQuantityComponent', () => {
  let component: RecycleQuantityComponent;
  let fixture: ComponentFixture<RecycleQuantityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecycleQuantityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecycleQuantityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
