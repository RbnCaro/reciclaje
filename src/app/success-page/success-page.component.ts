import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-page',
  templateUrl: './success-page.component.html',
  styleUrls: ['./success-page.component.css']
})
export class SuccessPageComponent implements OnInit {
  msgs = [
    {severity:'success', summary:'Exito', detail:'El retiro se ha confirmado'}
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
