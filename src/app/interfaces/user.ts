export interface User {
  id?: string;
  name: string;
  phone: string;
  lastName: string;
  email: string;
  password: string;
}