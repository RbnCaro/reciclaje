import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-confirmation-request',
  templateUrl: './confirmation-request.component.html',
  styleUrls: ['./confirmation-request.component.css']
})
export class ConfirmationRequestComponent implements OnInit {
  paperboard: number = 0;
  plastic: number = 0;
  glass: number = 0;
  can: number = 0;
  schedule: string;
  date: string;

  constructor(private store: AngularFirestore, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.paperboard = localStorage.getItem('paperboard') ? Number(localStorage.getItem('paperboard')) : 0;
    this.plastic = localStorage.getItem('plastic') ? Number(localStorage.getItem('plastic')) : 0;
    this.glass = localStorage.getItem('glass') ? Number(localStorage.getItem('glass')) : 0;
    this.can = localStorage.getItem('can') ? Number(localStorage.getItem('can')) : 0;
    this.schedule = localStorage.getItem("schedule");
    this.date = moment(localStorage.getItem("date")).format("DD/MM/YYYY");
  }

  save() {
    const data = {
      paperboard: this.paperboard,
      plastic: this.plastic,
      glass: this.glass,
      can: this.can,
      date: this.date,
      schedule: this.schedule,
      state: 'created',
      address: "Eusebio Lillo 356",
      userId: localStorage.getItem('userId'),
    }
    this.store.collection("recycle-pickup").add(data);
    let user = JSON.parse(localStorage.getItem('user'));
    let id = localStorage.getItem('userId')
    this.store.collection('/users').doc(id).set({
      ...user,
      points: user.points + ((this.paperboard * 5) + (this.plastic * 5) + (this.glass * 5) + (this.can * 5))
    })
    this.router.navigate([{ outlets: { asd: ["exito"] } }], { relativeTo: this.activatedRoute.parent });
  }

}
