import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterOutlet } from '@angular/router';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { slideInAnimation } from './../animations';


@Component({
  selector: 'app-prinicpal',
  templateUrl: './prinicpalc.component.html',
  styleUrls: ['./prinicpal.component.css'],
  animations: [
    slideInAnimation
  ]
})
export class PrinicpalComponent implements OnInit {
  paperboard: number = 0;
  plastic: number = 0;
  glass: number = 0;
  can: number = 0;
  items: MenuItem[];
  selectedMenu: string = "inicio";
  profile: string = localStorage.getItem('profile');

  constructor(private router: Router, private message: MessageService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.items = [{
      label: 'File',
      items: [
        { label: 'New', icon: 'pi pi-plus' },
        { label: 'Open', icon: 'pi pi-download' }
      ]
    },
    {
      label: 'Edit',
      items: [
        { label: 'Undo', icon: 'pi pi-refresh' },
        { label: 'Redo', icon: 'pi pi-repeat' }
      ]
    }];
    this.router.navigate([{ outlets: { asd: ["inicio"] } }], { relativeTo: this.activatedRoute });
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  navigateTo(route: string) {
    this.selectedMenu = route;
    this.router.navigate([{ outlets: { asd: [route] } }], { relativeTo: this.activatedRoute });
  }

  goToDate() {
    let total: number = Number(this.paperboard) + Number(this.plastic) + Number(this.glass) + Number(this.can);
    if (total < 20) {
      this.message.add({
        severity: "warn",
        summary: "No hay suficientes items",
        detail: "Cantidad mínima a reciclar: 20 items\n Cantidad actual: " + total,
        closable: false,
        life: 3000
      });
      return;
    }
    localStorage.removeItem('paperboard');
    localStorage.removeItem('plastic');
    localStorage.removeItem('glass');
    localStorage.removeItem('can');
    if (this.paperboard != 0) {
      localStorage.setItem('paperboard', this.paperboard.toString());
    }
    if (this.plastic != 0) {
      localStorage.setItem('plastic', this.plastic.toString());
    }
    if (this.glass != 0) {
      localStorage.setItem('glass', this.glass.toString());
    }
    if (this.can != 0) {
      localStorage.setItem('can', this.can.toString());
    }
    this.router.navigate(['/agendar-reciclaje']);
  }

}
