import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-recycle-choose',
  templateUrl: './recycle-choose.component.html',
  styleUrls: ['./recycle-choose.component.css'],
  providers: [ConfirmationService]
})
export class RecycleChooseComponent implements OnInit {

  recycles = [];
  loading: boolean = true;

  constructor(private store: AngularFirestore, private confirmationService: ConfirmationService, private message: MessageService) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.loading = true;
    this.recycles = [];
    let result = this.store.collection('/recycle-pickup', ref => ref.where('state', '==', 'created')).snapshotChanges();
    result.subscribe((recycles: any) => {
      recycles.forEach(recycle => {
        this.recycles.push({
          ...recycle.payload.doc.data(),
          id: recycle.payload.doc.id
        });
      });
      this.recycles = this.recycles.reverse();
      this.loading = false;
    });
  }

  confirmReception(recycle) {
    console.log(JSON.parse(localStorage.getItem("user")));
    this.confirmationService.confirm({
      message: '¿Estás seguro/a que deseas aceptar el pedido?',
      header: 'Confirmación',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store.collection('/recycle-pickup').doc(recycle.id).set({
          ...recycle,
          state: 'waiting',
          user: JSON.parse(localStorage.getItem("user"))
        }).then(() => {
          this.message.add({
            severity: "success",
            summary: "'Exito'",
            detail: "La recepción se ha realizado con éxito",
            closable: false,
            life: 1500
          });
          this.loadData();
        })
      },
    });
  }

}
