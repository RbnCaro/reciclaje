import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecycleChooseComponent } from './recycle-choose.component';

describe('RecycleChooseComponent', () => {
  let component: RecycleChooseComponent;
  let fixture: ComponentFixture<RecycleChooseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecycleChooseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecycleChooseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
