import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleRecycleComponent } from './schedule-recycle.component';

describe('ScheduleRecycleComponent', () => {
  let component: ScheduleRecycleComponent;
  let fixture: ComponentFixture<ScheduleRecycleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleRecycleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleRecycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
