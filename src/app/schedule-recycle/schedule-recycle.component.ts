import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-schedule-recycle',
  templateUrl: './schedule-recycle.component.html',
  styleUrls: ['./schedule-recycle.component.css']
})
export class ScheduleRecycleComponent implements OnInit {

  selectedValue: string;
  date: string;

  constructor(private router: Router, private store: AngularFirestore, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

  confirm() {
    let paperboard = localStorage.getItem('paperboard');
    let plastic = localStorage.getItem('plastic');
    let glass = localStorage.getItem('glass');
    let can = localStorage.getItem('can');
    
    const data = {
      paperboard: paperboard,
      plastic: plastic,
      glass: glass,
      can: can,
      date: moment(this.date).format("YYYY-MM-DD"),
      schedule: this.selectedValue
    }
    localStorage.setItem("date", moment(this.date).format("YYYY-MM-DD"));
    localStorage.setItem("schedule", this.selectedValue);
    // this.store.collection("recycle-pickup").add(data);
    // this.router.navigate(['/confirmacion-pedido']);
    this.router.navigate([{ outlets: { asd: ["confirmacion-pedido"] } }], { relativeTo: this.activatedRoute.parent });
  }
}
