import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  items: MenuItem[];
  basicData: any;
  recycleData: any;
  basicOptions: any;

  constructor() { }

  ngOnInit() {
    this.items = [
      {
        label: 'Reportes',
        icon: 'pi pi-fw pi-chart-line'
      },
      {
        label: 'Usuarios',
        icon: 'pi pi-fw pi-users'
      },
      {
        label: 'Cerrar sesión',
        icon: 'pi pi-fw pi-power-off',
        
      }
    ];
    this.basicData = {
      labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'],
      datasets: [
        {
          label: 'Latas',
          backgroundColor: '#42A5F5',
          data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: 'Cartón',
          backgroundColor: '#FFA726',
          data: [28, 48, 40, 19, 86, 27, 90]
        },
        {
          label: 'Vidrio',
          backgroundColor: '#44e46e ',
          data: [55, 49, 70, 71, 36, 15, 30]
        },
        {
          label: 'Plástico PET',
          backgroundColor: '#e45c5c',
          data: [58, 18, 24, 39, 16, 77, 50]
        }
      ]
    };
    this.recycleData = {
      labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio'],
      datasets: [
        {
          label: 'Pedidos',
          backgroundColor: '#42A5F5',
          data: [65, 59, 80, 81, 56, 55, 40]
        }
      ]
    };
    this.basicOptions = {
      legend: {
        labels: {
          fontColor: '#495057'
        }
      },
      scales: {
        xAxes: [{
          ticks: {
            fontColor: '#495057'
          },
          gridLines: {
            color: '#dedede'
          }
        }],
        yAxes: [{
          ticks: {
            fontColor: '#495057'
          },
          gridLines: {
            color: '#dedede'
          }
        }]
      }
    }

  }
}