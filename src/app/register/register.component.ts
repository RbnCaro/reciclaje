import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-register',
  templateUrl: './registerc.component.html',
  styleUrls: ['./register.component.css'],
  // providers: [MessageService]
})
export class RegisterComponent implements OnInit {

  name: string;
  lastName: string;
  email: string;
  cellphone: string;
  address: string;
  password: string;
  passwordConfirmation: string;
  isLoading: boolean = false;

  constructor(private router: Router, private store: AngularFirestore, private message: MessageService) { }

  ngOnInit(): void {
  }

  createAccount(): void {
    
    if (!this.name || !this.lastName || !this.email || !this.password) {
      this.message.add({
        severity: "warn",
        summary: "Campos faltantes",
        detail: "Por favor complete los campos",
        closable: false,
        life: 2000
      });
      return;
    }
    if (this.password !== this.passwordConfirmation) {
      this.message.add({
        severity: "warn",
        summary: "Validación",
        detail: "Las contraseñas no coinciden",
        closable: false,
        life: 2000
      });
      return;
    }
    const data = {
      name: this.name,
      lastName: this.lastName,
      email: this.email,
      phone: this.cellphone,
      password: this.password,
      address: this.address,
      commune: 'Punta Arenas',
      region: 'Magallanes',
      profile: localStorage.getItem('profileSelected')
    }
    this.store.collection("users").add(data);
    this.message.add({
      severity: "success",
      summary: "Éxito!",
      detail: "Usuario correctamente creado",
      closable: false,
      life: 3000
    });
    this.router.navigate(['/']);
  }

  back(): void {
    this.router.navigate(['choose-profile']);
  }

}
