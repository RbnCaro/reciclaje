import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';

import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import {DividerModule} from 'primeng/divider';
import {CalendarModule} from 'primeng/calendar';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';

import { AppRoutingModule } from './app-routing.module';
import { PrinicpalComponent } from './prinicpal/prinicpal.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { RegisterComponent } from './register/register.component'
import {RadioButtonModule} from 'primeng/radiobutton';
import {MenuModule} from 'primeng/menu';
import {CardModule} from 'primeng/card';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {MenubarModule} from 'primeng/menubar';
import {ChartModule} from 'primeng/chart';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ScheduleRecycleComponent } from './schedule-recycle/schedule-recycle.component';
import { SuccessPageComponent } from './success-page/success-page.component';
import { ConfirmationRequestComponent } from './confirmation-request/confirmation-request.component';
import { RecycleQuantityComponent } from './recycle-quantity/recycle-quantity.component';
import { UserDataComponent } from './user-data/user-data.component';
import { HomeComponent } from './home/home.component';
import { ModifyUserDataComponent } from './modify-user-data/modify-user-data.component';
import { ChooseProfileComponent } from './choose-profile/choose-profile.component';
import { HomePickupsComponent } from './home-pickups/home-pickups.component';
import { RecycleChooseComponent } from './recycle-choose/recycle-choose.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
// import { RecycleComponent } from './recycle/recycle.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PrinicpalComponent,
    RegisterComponent,
    ScheduleRecycleComponent,
    SuccessPageComponent,
    ConfirmationRequestComponent,
    RecycleQuantityComponent,
    UserDataComponent,
    HomeComponent,
    ModifyUserDataComponent,
    ChooseProfileComponent,
    HomePickupsComponent,
    RecycleChooseComponent,
    AdminPanelComponent,
    // RecycleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,

    ProgressSpinnerModule,
    InputTextModule,
    ButtonModule,
    ToastModule,
    DividerModule,
    CalendarModule,
    RadioButtonModule,
    MessagesModule,
    MessageModule,
    MenuModule,
    CardModule,
    ConfirmDialogModule,
    MenubarModule,
    ChartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
