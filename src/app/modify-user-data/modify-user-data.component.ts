import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-modify-user-data',
  templateUrl: './modify-user-data.component.html',
  styleUrls: ['./modify-user-data.component.css']
})
export class ModifyUserDataComponent implements OnInit {
  user: any = {
    name: "",
    lastName: "",
    email: "",
    phone: ""
  };
  id: string;
  isLoading: boolean = false;

  constructor(private router: Router, private store: AngularFirestore, private activatedRoute: ActivatedRoute, private message: MessageService) { }

  ngOnInit(): void {
    this.id = localStorage.getItem("userId");
    // let result = this.store.collection('/users', ref => ref.where('email', '==', email)).valueChanges();
    let result = this.store.collection('/users').doc(this.id).snapshotChanges();
    result.subscribe((user) => {
      
      this.user = user.payload.data();
      // this.id = user.payload.doc.id;
      // this.name = user.name;
      // this.lastName = user.lastName;
      // this.email = user.email;
      // this.phone = user.phone;
    });
  }

  back() {
    this.router.navigate([{ outlets: { asd: ["user-data"] } }], { relativeTo: this.activatedRoute.parent });
  }

  modifyData() {
    this.isLoading = true;
    this.store.collection('/users').doc(this.id).set({
      ...this.user
    }).then(() => {
      console.log('finish')
      this.isLoading = false;
      this.message.add({
        severity: "success",
        summary: "'Exito'",
        detail: "Los datos se han actualizado",
        closable: false,
        life: 1500
      });
    })
  }
}
