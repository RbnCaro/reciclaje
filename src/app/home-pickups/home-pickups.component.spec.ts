import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePickupsComponent } from './home-pickups.component';

describe('HomePickupsComponent', () => {
  let component: HomePickupsComponent;
  let fixture: ComponentFixture<HomePickupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomePickupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePickupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
